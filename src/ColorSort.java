import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
//      Color[] testing = {Color.blue, Color.green, Color.red, Color.blue, Color.red, Color.blue, Color.green};
//      Color[] testing = {Color.green, Color.green, Color.green, Color.green, Color.green, Color.green};
      Color[] testing = {Color.blue, Color.green, Color.red, Color.blue, Color.red, Color.blue, Color.red};

      reorder(testing);
      for (Color ball : testing) {
         System.out.println(ball);
      }
   }
   
   public static void reorder (Color[] balls) {
      //      Arrays.sort(balls, Comparator.comparingInt(Enum::ordinal));

//      Color[] redBalls =  new Color[balls.length];
//      Color[] greenBalls = new Color[balls.length];
//      Color[] blueBalls =  new Color[balls.length];
//
//      int reds = 0;
//      int greens = 0;
//      int blues = 0;
//
//      for (Color ball : balls) {
//         if (ball == Color.red) {
//            redBalls[reds] = ball;
//            reds++;
//         } else if (ball == Color.green) {
//            greenBalls[greens] = ball;
//            greens++;
//         } else {
//            blueBalls[blues] = ball;
//            blues++;
//         }
//      }
//
//      if (reds == balls.length || greens == balls.length || blues == balls.length) {
//         return;
//      }
//
//      int ballsIndex = 0;
//
//      for (int i = 0; i < reds; i++) {
//         balls[ballsIndex] = redBalls[i];
//         ballsIndex++;
//      }
//      for (int i = 0; i < greens; i++) {
//         balls[ballsIndex] = greenBalls[i];
//         ballsIndex++;
//      }
//      for (int i = 0; i < blues; i++) {
//         balls[ballsIndex] = blueBalls[i];
//         ballsIndex++;
//      }

//      Color[] cache = Arrays.copyOf(balls, balls.length);
//      Color[] greensBalls = new Color[balls.length];
//
//      int reds = 0;
//      int greens = 0;
//      int blues = 0;
//
//      int indexBlue = balls.length - 1;
//
//      for (Color ball : cache) {
//         if (ball == Color.red) {
//            balls[reds] = ball;
//            reds++;
//         } else if (ball == Color.green) {
//            greensBalls[greens] = ball;
//            greens++;
//         } else {
//            blues++;
//            balls[indexBlue] = ball;
//            indexBlue--;
//         }
//      }
//
//      if (reds == balls.length || greens == balls.length || blues == balls.length) {
//         return;
//      }
//
//      int limit = balls.length - reds - blues;
//      for (int i = 0; i < limit; i++) {
//         balls[reds] = greensBalls[i];
//         reds++;
//      }

      int reds = 0;
      int greens = 0;

      for (Color ball : balls) {
         if (ball == Color.red) {
            reds++;
         } else if (ball == Color.green) {
            greens++;
         }
      }
      if (reds == balls.length || greens == balls.length || balls.length - reds - greens == balls.length) {
         return;
      }

      for (int i = 0; i < reds; i++) {
         balls[i] = Color.red;
      }
      for (int i = reds; i < reds + greens; i++) {
         balls[i] = Color.green;
      }
      for (int i = reds + greens; i < balls.length; i++) {
         balls[i] = Color.blue;
      }
   }
}
